#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D			#
# Emory University, Dept. of Human Genetics	#
# ashok.reddy.dinasarapu@emory.edu		#
# Date: 03/02/2019				#
#################################################

# 1. Rarefy a feature table
# 2. Alpha and Beta diversity

#$ -N diversity
#$ -q b.q
#$ -pe smp 1
#$ -cwd
#$ -j y
#$ -m abe
#$ -M user_name@emory.edu
 
# activate qiime2 environment
module load R/3.5.2
module load Anaconda3/5.2.0
source activate qiime2-2019.1

# main project directory 
PROJ_DIR=$HOME/metagenomics
 
# create a directory and a sub-directory for output data
OUT_DIR=$PROJ_DIR/out/data_diversity
if [ ! -d ${OUT_DIR} ]; then
  mkdir -p ${OUT_DIR}
fi
 
export TMPDIR=/scratch
 
if [ -e /bin/mktemp ]; then
  TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
  TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
  echo “Error. Cannot find mktemp to create tmp directory”
  exit
fi

chmod a+trwx $TMP_DIR
 
# Filtered data directory 
FILTER_DIR=$PROJ_DIR/out/data_filtered

# copy metadata, feature table and sequences
rsync -av $PROJ_DIR/data/ibs.microbiome/metadata_file.txt $TMP_DIR/metadata_file.txt
# rsync -av $FILTER_DIR/sample_filtered_rep_seq.qza $TMP_DIR/sample_filtered_rep_seq.qza
rsync -av $FILTER_DIR/sample_filtered_table.qza $TMP_DIR/sample_filtered_table.qza

#===========================#
# 1. Rarefy a feature table #
#===========================#

# Each column is subsampled to even depth without replacement (hypergeometric model)
# Correction for unequal library sizes is applied
# It is a single random draw.

qiime feature-table rarefy \
 --i-table $TMP_DIR/sample_filtered_table.qza \
 --p-sampling-depth 20327 \
 --o-rarefied-table $TMP_DIR/rarefied_table.qza

#====================#
# 2. Alpha Diversity #
#====================#

# Alpha and beta diversity analyses, or measures of diversity within and between samples, respectively 
# (a.k.a., “how similar are my samples?”)

# Computes a user-specified alpha diversity metric for all samples in a feature table
# At the moment you can only supply one metric at a time 

/bin/mkdir -p $TMP_DIR/alpha_diversity

qiime diversity alpha \
 --i-table $TMP_DIR/rarefied_table.qza \
 --p-metric shannon \
 --output-dir $TMP_DIR/alpha_diversity/shannon

# Visually and statistically compare groups of alpha diversity values
qiime diversity alpha-group-significance \
 --i-alpha-diversity $TMP_DIR/alpha_diversity/shannon/alpha_diversity.qza \
 --m-metadata-file $TMP_DIR/metadata_file.txt \
 --o-visualization $TMP_DIR/alpha_diversity/shannon/alpha_group_significance.qzv

#===================#
# 3. Beta Diversity #
#===================#

# Computes a user-specified beta diversity metric for all pairs of samples in a feature table.
# All beta diversity metrics use OTU frequencies or presence / absence, neither of which can be reliably determined from amplicon reads.

/bin/mkdir -p $TMP_DIR/beta_diversity
   
qiime diversity beta \
 --i-table $TMP_DIR/rarefied_table.qza \
 --p-metric braycurtis \
 --output-dir $TMP_DIR/beta_diversity/braycurtis

# Determine whether groups of samples are significantly different from one
# another using a permutation-based statistical test
qiime diversity beta-group-significance \
 --i-distance-matrix $TMP_DIR/beta_diversity/braycurtis/distance_matrix.qza \
 --m-metadata-file $TMP_DIR/metadata_file.txt \
 --m-metadata-column SubjectGroup \
 --p-permutations 999 \
 --p-method permanova \
 --p-pairwise \
 --o-visualization $TMP_DIR/beta_diversity/braycurtis/beta_group_significance.qzv

# Apply principal coordinate analysis
qiime diversity pcoa \
 --i-distance-matrix $TMP_DIR/beta_diversity/braycurtis/distance_matrix.qza \
 --o-pcoa $TMP_DIR/beta_diversity/braycurtis/braycurtis_pcoa.qza

# Generate visualization of your ordination
qiime emperor plot \
 --i-pcoa $TMP_DIR/beta_diversity/braycurtis/braycurtis_pcoa.qza \
 --m-metadata-file $TMP_DIR/metadata_file.txt \
 --o-visualization $TMP_DIR/beta_diversity/braycurtis/braycurtis_pcoa_emperor.qzv

# delete all uploaded files before you copy results
/bin/rm $TMP_DIR/sample_filtered_table.qza
/bin/rm $TMP_DIR/metadata_file.txt

# Copy results back to your home directory
rsync -av $TMP_DIR/* $OUT_DIR

# Finally, delete tmp directory
/bin/rm -rf $TMP_DIR

# unload R, anaconda3 and qiime2
module unload R/3.5.2
module unload Anaconda3/5.2.0
source deactivate qiime2-2019.1

#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D                      #
# Emory University, Dept. of Human Genetics     #
# ashok.reddy.dinasarapu@emory.edu              #
# Last updated on 03/02/2019                    #
#################################################

#$ -N import
#$ -q b.q
#$ -pe smp 1
#$ -cwd
#$ -j y
#$ -m abe
#$ -M user_name@emory.edu

# activate qiime2 environment
module load R/3.5.2
module load Anaconda3/5.2.0 
source activate qiime2-2019.1

# main project directory
PROJ_DIR=$HOME/metagenomics

# create a directory and a sub-directory for output data
OUT_DIR=$PROJ_DIR/out/data_imported
if [ ! -d ${OUT_DIR} ]; then
 mkdir -p ${OUT_DIR}
fi

export TMPDIR=/scratch

if [ -e /bin/mktemp ]; then
 TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

# fastq data directory
FASTQ_DIR=$PROJ_DIR/data/ibs.microbiome

# copy all fastq.gz files to node tmp directory
rsync -av $FASTQ_DIR/*_R[1-2]_001.fastq.gz $TMP_DIR

ls -la $TMP_DIR/

##########################
# Create a manifest file #
########################## 
# with header
# sample-id,absolute-filepath,direction
echo -e "sample-id,absolute-filepath,direction" >> $TMP_DIR/manifest.txt

for entry in $TMP_DIR/*.fastq.gz; do

 # file name
 fname=$(basename $entry)

 # split fastq.gz file name before "_R" character (change this if necessary)
 fbname=${fname%_R*}

 # makesure to check both forward and reverse read files exist
 # fcount=`ls $TMP_DIR/ | grep $fbname$ | wc -l`
 fcount=`ls $TMP_DIR/ | xargs -n1 basename | awk 'BEGIN{FS="_"}{ print $1 }' | grep $fbname$ | wc -l`
 # echo $paired_files
 # fcount=$(echo $paired_files | wc -l)

 echo $fbname $fcount

 if [[ ${fcount} == 2 ]]; then
  # old version - Don't use underscores (_) in sample-id of manifest file 
  #fbname=`echo $fbname | sed 's/_/-/'`
  fpre="$fbname,$TMP_DIR/$fname"
  if [[ $entry == *"_R1_"* ]]; then
   echo -e "$fpre,forward" >> $TMP_DIR/manifest.txt
  fi
  if [[ $entry == *"_R2_"* ]]; then
   echo -e "$fpre,reverse" >> $TMP_DIR/manifest.txt
  fi
 fi
done

cat $TMP_DIR/manifest.txt

#=====================================================#
# Import FASTQ files to create a new QIIME 2 Artifact #
#=====================================================#
# --type	  : The semantic type of the artifact that will be created upon importing
# --source-format : The format of the data to be imported

qiime tools import \
 --type SampleData[PairedEndSequencesWithQuality] \
 --input-path $TMP_DIR/manifest.txt \
 --output-path $TMP_DIR/paired_end_data.qza \
 --input-format PairedEndFastqManifestPhred33

# Summarize counts per sample for all samples
# Plot positional qualitites 
# --p-n : The number of sequences that should be selected at random for quality score plots

qiime demux summarize \
 --i-data $TMP_DIR/paired_end_data.qza \
 --p-n 10000 \
 --o-visualization $TMP_DIR/imported_data_qualities.qzv

# before copying results, remove all uploaded raw data
/bin/rm $TMP_DIR/*.fastq.gz

# copying results
rsync -av $TMP_DIR/* $OUT_DIR

# After copying results, remove tmp directory
/bin/rm -rf $TMP_DIR

# unload R, anaconda3 and qiime2
module unload R/3.5.2
module unload Anaconda3/5.2.0
source deactivate qiime2-2019.1

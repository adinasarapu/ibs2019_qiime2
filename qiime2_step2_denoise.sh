#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D                      #
# Emory University, Dept. of Human Genetics     #
# ashok.reddy.dinasarapu@emory.edu              #
# Last updated on 03/02/2019                    #
#################################################

#$ -N denoise
#$ -q b.q
#$ -pe smp 6
#$ -cwd
#$ -j y
#$ -m abe
#$ -M user_name@emory.edu

# activate qiime2 environment
module load R/3.5.2
module load Anaconda3/5.2.0
source activate qiime2-2019.1

# directory list
PROJ_DIR=$HOME/metagenomics

 # create a directory and a sub-directory for output data
OUT_DIR=$PROJ_DIR/out/data_denoised
if [ ! -d ${OUT_DIR} ]; then
  mkdir -p ${OUT_DIR}
fi

export TMPDIR=/scratch

if [ -e /bin/mktemp ]; then
 TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi
 
# imported data (qiime2 artifact) directory
IMPORT_DIR=$PROJ_DIR/out/data_imported

# copy qiime2 artifact and metadata files to node tmp directory
rsync -av $IMPORT_DIR/paired_end_data.qza $TMP_DIR/paired_end_data.qza
rsync -av $PROJ_DIR/data/ibs.microbiome/metadata_file.txt $TMP_DIR/metadata_file.txt

# create a sub-directory
#chmod a+trwx $TMP_DIR
#/usr/bin/mkdir -p $TMP_DIR/dada2

qiime dada2 denoise-paired \
 --i-demultiplexed-seqs $TMP_DIR/paired_end_data.qza \
 --output-dir $TMP_DIR/dada2 \
 --p-n-threads 6 \
 --p-chimera-method pooled --p-min-fold-parent-over-abundance 1.0 \
 --p-hashed-feature-ids --p-max-ee 2.0 --p-trunc-q 2 --p-n-reads-learn 1000000 \
 --p-trim-left-f 20 \
 --p-trim-left-r 20 \
 --p-trunc-len-f 290 \
 --p-trunc-len-r 290

# denoising stats visualization
qiime metadata tabulate \
 --m-input-file $TMP_DIR/dada2/denoising_stats.qza \
 --o-visualization $TMP_DIR/dada2/denoising_stats.qzv

# Generates visual and tabular summaries of a feature table
qiime feature-table summarize \
 --i-table $TMP_DIR/dada2/table.qza \
 --o-visualization $TMP_DIR/dada2/table.qzv \
 --m-sample-metadata-file $TMP_DIR/metadata_file.txt
 
# View sequence associated with each feature
 qiime feature-table tabulate-seqs \
 --i-data $TMP_DIR/dada2/representative_sequences.qza \
 --o-visualization $TMP_DIR/dada2/representative_sequences.qzv

# remove uploaded data before copy results
/bin/rm $TMP_DIR/paired_end_data.qza
/bin/rm $TMP_DIR/metadata_file.txt

# copy results
rsync -av $TMP_DIR/dada2/* $OUT_DIR

# delete tmp directory
/bin/rm -rf $TMP_DIR

# unload R, anaconda3 and qiime2
module unload R/3.5.2
module unload Anaconda3/5.2.0
source deactivate qiime2-2019.1

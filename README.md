# IBS 574 - Computational Biology & Bioinformatics, Spring 2019, Emory University

# Microbiome (16S rRNA) Exercise: raw reads to taxonomic classification     

The following pipeline is intended to help Emory University (IBS-574 class) students in analyzing 16S rRNA sequencing data via QIIME2 pipeline [^1]. A basic knowledge of the usage of the Linux shell is required [^2].

A typical workflow for 16S rRNA amplicon data analysis consists of the following steps:  

 * Connect to Emory's Human Genetics Computing Cluster (HGCC)  
 * Create project directories  
 * Get raw data and taxonomy database  
 * Run the following scripts:  
```
qiime2_step1_import.sh  
qiime2_step2_denoise.sh  
qiime2_step3_filter.sh 
qiime2_step4_phylogeny.sh  
qiime2_step5_diversity.sh  
qiime2_step6_taxonomy.sh  
```

## Let’s go through each step to get a better idea of what it really means.  

## 1. Connect to the Human Genetics Computing Cluster (HGCC)[^3] [^4]  
SSH allows you to connect to the Human Genetics Compute Cluster (HGCC) server securely and perform LINUX command-line operations. For Windows users [PuTTY](https://www.putty.org) is a free SSH client.  

```
ssh <user_name>@hgcc.genetics.emory.edu
```  

Alternatively, open a web browser (Safari or Google Chrome) and enter the following URL into your browser address bar, then press enter.  

```
https://hgcc.genetics.emory.edu:22443  
```

## 2. Install and activate QIIME2 (Version: 2019.1)  
Installation instructions for [Anaconda, Python 3.7](https://www.anaconda.com/distribution/) and [QIIME2, 2019.1](https://docs.qiime2.org/2019.1/install/native/). Please see the guide on how to use Anaconda on [HGCC](https://hgcc.genetics.emory.edu/) for more information.

 **Anaconda** installation  
```
wget https://repo.anaconda.com/archive/Anaconda3-2018.12-Linux-x86_64.sh
sh Anaconda3-2018.12-Linux-x86_64.sh

Do you accept the license terms? [yes|no]  
[no] >>> yes  (Press ENTER)

Anaconda3 will now be installed into this location:
/home/adinasarapu/anaconda3 (Press ENTER to confirm the location)
 
[/home/adinasarapu/anaconda3] >>> 
PREFIX=/home/adinasarapu/anaconda3
```

 **QIIME2 2019.1** installation  
```
conda update conda

wget https://data.qiime2.org/distro/core/qiime2-2019.1-py36-linux-conda.yml
conda env create -n qiime2-2019.1 --file qiime2-2019.1-py36-linux-conda.yml

source activate qiime2-2019.1
```

Alternatively, to activate `qiime2` use  

```
module load R/3.5.2
module load Anaconda3/5.2.0 
source activate qiime2-2019.1
```
To deactivate qiime2  

```
module unload R/3.5.2
module unload Anaconda3/5.2.0
source activate qiime2-2019.1
```

## 3. (Optional) Install FTP client on your personal computer  
Install FileZilla (or a similar client application), which is a software for transferring files over the Internet.[^5]  

 * Open FileZilla  
 * Click on Open the Site Manager icon!
 * Enter Host (hgcc.genetics.emory.edu)    
 * Select Protocol (SFTP)  
 * Enter your User name    
 * Enter your password  
 * Click Connect when you get a warning about an unknown host key.  

## 4. Create a project directory and sub-directories  
From your home directory i.e /home/<user_name>  

```
mkdir -p metagenomics/{data,out,logs}  
```
Note: no spaces between data,out,logs

## 5. Get the (demultiplexed) raw data  
Copy raw data into `~/metagenomics/data` directory and extract the tar.gz file  
```
cp /scratch/ibs.microbiome.tar.gz ~/metagenomics/data  
cd ~/metagenomics/data
tar -xvzf ibs.microbiome.tar.gz  
```

Make sure the following (66) `fastq.gz` files and a `txt` file really exist in `~/metagenomics/data/ibs.microbiome`  

```
SDE11844-0009-1-1-A9_R1_001.fastq.gz  
Empty-1_R1_001.fastq.gz  
NTC-2_R2_001.fastq.gz  
Zymo-Positive-2_R2_001.fastq.gz  
...  
...  

metadata_file.txt  
```

## 6. Get the HOMD (Human Oral Microbiome Database) taxonomy database  
Copy HOMD[^6] database (for taxonomy) into `~/metagenomics` directory and extract the tar.gz file 
```
cp /scratch/HOMD_16S_rRNA_RefSeq.tar.gz ~/metagenomics
cd ~/metagenomics
tar -xvzf HOMD_16S_rRNA_RefSeq.tar.gz  
```

Make sure the following two files really exist in `~/metagenomics/HOMD_16S_rRNA_RefSeq`  
```
HOMD_16S_rRNA_RefSeq_V15.1.fasta  
HOMD_16S_rRNA_RefSeq_V15.1.qiime.taxonomy  
```

## 7. Get the QIIME2 warapper scripts  
Copy scripts into `~/metagenomics` directory and extract the tar.gz file
```
cp /scratch/scripts.tar.gz ~/metagenomics
cd ~/metagenomics
tar -xvzf scripts.tar.gz
```
 
Make sure the following two files really exist in `~/metagenomics/scripts`  
```
qiime2_step2_denoise.sh  
qiime2_step4_phylogeny.sh
qiime2_step6_taxonomy.sh
qiime2_step1_import.sh  
qiime2_step3_filter.sh
qiime2_step5_diversity.sh
```

Understand Queues and Parallel Environments  
```
qconf -sql  
qconf -sq <queue_name>  

qconf -spl  
qconf -sp <parallel environment name>  
```

Update the following lines from all of the above qiime2 scripts    
```
#$ -q ibs574
#$ -M user_name@emory.edu 
```

## 8. (Optional) Create a manifest file  
For your convenience, I have included a script in `qiime2_step1_import.sh` file, which automatically creates a manifest file (`manifest.txt`).  

```
sample-id,absolute-filepath,direction
sample-1,$PWD/some/filepath/sample1_R1.fastq.gz,forward
sample-2,$PWD/some/filepath/sample2_R1.fastq.gz,forward
sample-1,$PWD/some/filepath/sample1_R2.fastq.gz,reverse
sample-2,$PWD/some/filepath/sample2_R2.fastq.gz,reverse
```

The fastq.gz absolute filepaths may contain environment variables (e.g., $HOME or $PWD). The above [example](https://docs.qiime2.org/2018.11/tutorials/importing/) illustrates a simple fastq manifest file for paired-end read data for two samples.  

## 9. (Optional) Create a metadata file  
For your convenience, I have created the metadata file (`metadata_file.txt`) and provided it along with the raw data (`ibs.microbiome.tar.gz`).  

Metadata is information about your samples i.e samples as rows and variables as columns. Create a .tsv (or .txt) version of your metadata spreadsheet. See [Metadata](https://docs.qiime2.org/2019.1/tutorials/metadata/) formatting requirements.

To inspect the metadata file for correct formatting run the following command.  
```
qlogin  
module load R/3.5.2
module load Anaconda3/5.2.0  
source activate qiime2-2019.1  
qiime tools inspect-metadata metadata_file.txt  
```

Create a visualization of your metadata and view using QIIME2 Viewer (`.qzv` is a qiime zipped visualization).  
```
qiime metadata tabulate --m-input-file metadata_file.txt --o-visualization metadata_file.qzv  
```
Finally,  
```
source deactivate qiime2-2019.1  
exit
```  

## 10. Import (and quality check) the raw data  

Submit the script from `~/metagenomics/logs` sub-directory as  
```
qsub ~/metagenomics/scripts/qiime2_step1_import.sh
```  

If you submit it correctly, you may see a message like the following  
```
hgcc:node00:[logs] % qsub ~/metagenomics/scripts/qiime2_step1_import.sh  
Your job 300838 ("import") has been submitted
```

To check the status of all jobs submitted by you, use `qstat` command  
```
hgcc:node00:[logs] % qstat  
```

| job-ID | prior   | name   | user        | state | submit/start | at       | queue            | slots | ja-task-ID |      
| ------ | ------- | ------ | ----------- | ----- | ------------ | -------- | ---------------- | ----- | ---------- |    
| 300953 | 0.61633 | import | adinasarapu | r     | 02/24/2019   | 12:21:01 | b.q@node05.local | 1     | -          |      

The above script creates the following three files at (`~/metagenomics/out/data_imported/`)  

 * A manifest file (`manifest.txt`)
 * Raw data as a QIIME2 Artifact (`paired_end_data.qza`)
 * A visualization file with QC data (`imported_data_qualities.qzv`)  

## 11. Visualization of QIIME2 output  
Data produced by QIIME2 exists as QIIME 2 artifacts. A QIIME 2 artifact typically has the `.qza` file extension when stored in a file. Visualizations are another type of data (`.qzv` file extension) generated by QIIME2, which can be viewed using a web interface QIIME2 View (via Firefox web browser) without requiring a QIIME installation [^6]. Copy `imported_data_qualities.qzv` file to your local computer (using FTP client) and open it in Firefox using QIIME2 [View](https://view.qiime2.org).  

![Interactive quality plots](images/qc_plot.jpg)

## 12. Denoise the imported data using DADA2 (Run time, ~4:00 hours)  

Submit the script from `~/metagenomics/logs` sub-directory as  

```
qsub ~/metagenomics/scripts/qiime2_step2_import.sh  
```

The above script creates the following three files at (`~/metagenomics/out/data_denoised/`)

 * Denoising stats as a QIIME2 Artifact and its visualization (`denoising_stats.qza` and `denoising_stats.qzv`)
 * Denoised data (FeatureTable[Frequency]) as a QIIME2 Artifact (`table.qza` and `table.qzv`)
 * Representative sequences (FeatureData[Sequences]) as a QIIME2 Artifact (`representative_sequences.qza` and `representative_sequences.qzv`)   

The following table (based on `denoising_stats.qzv` file) includes the number of reads left after each step of the process: filtered reads based on Q score, denoised sequences, merged sequences, and after chimera filtering.  

| sample-id	   		     | input    | filtered | denoised | merged | non-chimeric |
| ---------------------------------- | -------- | -------- | -------- | ----------------------|
| SDE11844-0019-1-1-B10		     | 124622	| 45822	   | 45822    | 43655  | 40784        | 
| SDE11844-0018-1-1-B8		     | 85063	| 22539	   | 22539    | 21378  | 20376        |
| SDE11844-0021-1-Negative-Control-1 | 1895     | 522      | 522      | 449    | 449          |
| Zymo-Positive-1                    | 119426   | 44623    | 44623    | 43400  | 37445        |
| Zymo-Positive-2                    | 136208   | 50999    | 50999    | 49337  | 42598        |
| Zymo-Positive-3                    | 221120   | 83933    | 83933    | 82041  | 70625        |
| Zymo-Positive-4                    | 239136   | 98459    | 98459    | 95185  | 82287        |
| ...				     | ...      | ...      | ...      | ...    | ...          |
| ...				     | ...	| ...	   | ...      | ...    | ...	      |

## 13. Feature filtering and sample subsetting  

Submit the script from `~/metagenomics/logs` sub-directory as  

```
qsub ~/metagenomics/scripts/qiime2_step3_filter.sh
```

This script generates the following files at (`~/metagenomics/out/data_filtered/`)  
 
 * Singletons filtered table (`sample_filtered_table.qza` and `sample_filtered_table.qzv`)  
 * Filtered sequences (`sample_filtered_rep_seq.qza`)   

Singletons (and samples) filtered table summary - based on `sample_filtered_table.qzv`  

| Metric 	      | Sample       |
| ------------------- | ------------ |
| Number of samples   |	23           |
| Number of features  |	335          |
| Total frequency     |	1,068,983    |

`Sample-wise sequence counts`  

| Sample ID              | Sequence Count |
| ---------------------- | -------------- |
| Zymo-Positive-4        | 81,708         |
| SDE11844-0010-1-1-A10	 | 73,288         |
| SDE11844-0011-1-1-B1	 | 72,484         |
| ...                    | ...            |
| ...                    | ...            |
| ...			 | ...		  |
| SDE11844-0001-1-1-A1	 | 26,472         |
| SDE11844-0018-1-1-B8	 | 20,327         |

The sample with minimum counts is `SDE11844-0018-1-1-B8` (Sequence Count: `20,327`). Use this count as the sampling depth in subsequent (eg. rarefaction) analysis.

## 14. Build a phylogenetic tree 

This script produces a rooted tree and a rarefaction plot.  

 * Phylogenetic tree construction is optional, but is useful if you want to compute diversity metrics that are phylogenetically based.  
 * Produce a graphic that shows the impact of rarefaction (sampling at different depths) on alpha diversity of each sample. These are also called ‘collectors curves’.

Submit the script from `~/metagenomics/logs` sub-directory as  
```
qsub ~/metagenomics/scripts/qiime2_step4_phylogeny.sh
```  

## 15. Calculate diversity metrics  

Submit the script from `~/metagenomics/logs` sub-directory as  
```
qsub ~/metagenomics/scripts/qiime2_step5_diversity.sh
```

This script generates  
```
alpha_group_significance.qzv
braycurtis_pcoa_emperor.qzv
```  

## 16. Assign taxonomy to representative sequences  

Submit the script from `~/metagenomics/logs` sub-directory as  
```
qsub ~/metagenomics/scripts/qiime2_step6_taxonomy.sh  
```

The above script generates taxa plots. To visualize plots upload `taxa_bar_plots.qzv` file to QIIME 2 View application!  
 
 * **Taxonomic Level**: 6 (Genus) or 7 (Species) level  
 * **Sort Samples By**: SubjectGroup (metadata column) 

![Taxa barplots](images/taxa.jpg)


[^1]: [QIIME2](https://qiime2.org) - Quantitative Insights Into Microbial Ecology  
[^2]: [I](http://adinasarapu.github.io/files/2018_Linux_Shell_I.pdf) & [II](http://adinasarapu.github.io/files/2018_Linux_Shell_II.pdf), Linux shell & shell scripting
[^3]: [HGCC - job submission](http://wingolab.org/hgcc/), Emory's Human Genetics Compute Cluster
[^4]: [HGCC - support center](https://hgcc.genetics.emory.edu), Emory's Human Genetics Compute Cluster
[^5]: [HOMD](http://www.homd.org) - expanded Human Oral Microbiome Database (eHOMD)
[^6]: [HOMD](http://www.homd.org) - expanded Human Oral Microbiome Database (eHOMD)
[^7]: [QIIME2 View](https://view.qiime2.org) - Open in Firefox

## Note -   

**~** (tilde) is a Linux "shortcut" to denote a user's home directory.  
**~** is equal to **/home/<your_user_name>** directory.  
**~/** is equal to **/home/<your_user_name>/** directory.

Use correct program (**qsub**, **sh** or **Rscript**) and file path (**relative path** or **absolute path**) to run your script.
To submit a **batch job** to the HGCC queues, use the `qsub` command followed by the name of your SGE batch script file.
```
qsub qiime2_step1_import.sh
```
When submitting an **interactive job**, first **qlogin** and run the script. See section 9.  

## References

#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D			#
# Emory University, Dept. of Human Genetics	#
# ashok.reddy.dinasarapu@emory.edu		#
# Date: 03/02/2019				#
#################################################

#$ -N taxonomy
#$ -q b.q
#$ -pe smp 6
#$ -cwd
#$ -j y
#$ -m abe
#$ -M user_name@emory.edu
 
# activate qiime2 environment
module load R/3.5.2
module load Anaconda3/5.2.0
source activate qiime2-2019.1

# main project directory 
PROJ_DIR=$HOME/metagenomics
 
# create a directory and a sub-directory for output data
OUT_DIR=$PROJ_DIR/out/data_taxonomy
if [ ! -d ${OUT_DIR} ]; then
  mkdir -p ${OUT_DIR}
fi
 
export TMPDIR=/scratch
 
if [ -e /bin/mktemp ]; then
   TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
   TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
   echo “Error. Cannot find mktemp to create tmp directory”
   exit
fi
 
chmod a+trwx $TMP_DIR
 
# Filtered data directory 
FILTER_DIR=$PROJ_DIR/out/data_filtered
 
# copy metadata, feature table and sequences
rsync -av $PROJ_DIR/data/ibs.microbiome/metadata_file.txt $TMP_DIR/metadata_file.txt
rsync -av $FILTER_DIR/sample_filtered_rep_seq.qza $TMP_DIR/sample_filtered_rep_seq.qza
rsync -av $FILTER_DIR/sample_filtered_table.qza $TMP_DIR/sample_filtered_table.qza

#===========================#
# 1. Rarefy a feature table #
#===========================#

# Each column is subsampled to even depth without replacement (hypergeometric model)
# Correction for unequal library sizes is applied

echo -e "qiime feature-table rarefy with SAMPLING_DEPTH=20327"
qiime feature-table rarefy \
 --i-table $TMP_DIR/sample_filtered_table.qza \
 --p-sampling-depth 20327 \
 --o-rarefied-table $TMP_DIR/filtered_rarefied_table.qza

qiime feature-table filter-seqs \
 --i-table $TMP_DIR/sample_filtered_table.qza \
 --i-data $TMP_DIR/sample_filtered_rep_seq.qza \
 --o-filtered-data $TMP_DIR/filtered_rarefied_rep_seq.qza

#=================================================================================#
# 1. Taxonomic classification of sequences (a.k.a., “what species are present?”)  #
#=================================================================================#

# create sub dir in tmp
/bin/mkdir -p $TMP_DIR/taxonomy

TAX_DIR=$TMP_DIR/taxonomy/HOMD

#=================================================#
# expanded Human Oral Microbiome Database (eHOMD) #
#=================================================#
# http://www.ehomd.org/index.php?name=seqDownload&file&type=R
rsync -av $HOME/HOMD_16S_rRNA_RefSeq/HOMD_16S_rRNA_RefSeq_V15.1.fasta $TMP_DIR/db_otus.fasta
rsync -av $HOME/HOMD_16S_rRNA_RefSeq/HOMD_16S_rRNA_RefSeq_V15.1.qiime.taxonomy $TMP_DIR/db_otu_taxonomy.txt

if [ ! -d $TAX_DIR ]; then  
  /bin/mkdir -p $TAX_DIR
  echo "cretaed $TAX_DIR"
fi

# Extracting ref reads using 16S rRNA gene-specific primers
# 341F 5'-CCTACGGGNGGCWGCAG-3' and 805R 5'-GACTACHVGGGTATCTAATCC-3' 

fprimer="CCTACGGGNGGCWGCAG"
rprimer="GACTACHVGGGTATCTAATCC"

qiime tools import \
 --type 'FeatureData[Sequence]' \
 --input-path $TMP_DIR/db_otus.fasta \
 --output-path $TAX_DIR/db_otus.qza

qiime tools import \
 --type 'FeatureData[Taxonomy]' \
 --input-format HeaderlessTSVTaxonomyFormat \
 --input-path $TMP_DIR/db_otu_taxonomy.txt \
 --output-path $TAX_DIR/ref_taxonomy.qza

echo "Extract sequencing-like reads from a reference database "
qiime feature-classifier extract-reads \
 --i-sequences $TAX_DIR/db_otus.qza \
 --p-f-primer $fprimer \
 --p-r-primer $rprimer \
 --p-identity 0.8 \
 --p-trunc-len 290 \
 --p-trim-left 20 \
 --o-reads $TAX_DIR/ref_seqs.qza

echo "Creating a scikit-learn naive_bayes classifier for reads"
qiime feature-classifier fit-classifier-naive-bayes \
 --i-reference-reads $TAX_DIR/ref_seqs.qza \
 --i-reference-taxonomy $TAX_DIR/ref_taxonomy.qza \
 --o-classifier $TAX_DIR/classifier.qza

echo "Classifying reads by taxon using a fitted classifier"
qiime feature-classifier classify-sklearn \
 --i-classifier $TAX_DIR/classifier.qza \
 --i-reads $TMP_DIR/filtered_rarefied_rep_seq.qza \
 --p-n-jobs 5 \
 --p-confidence 0.7 \
 --o-classification $TAX_DIR/taxonomy.qza
  
# ============================#
# Filter feature contaminants #
#=============================#
# search for matching lines with grep then select the id column
# -i, --ignore-case; 
# -v --invert-match
grep -i "mitochondia|chloroplast|Feature" $TAX_DIR/export/taxonomy.tsv | \
cut -f 1 > $TAX_DIR/export/chloro_mito_ids.txt
   
echo "Filtering taxa contaminants from table, like mitochondria,chloroplast"
qiime taxa filter-table \
 --i-table $TMP_DIR/filtered_rarefied_table.qza \
 --i-taxonomy $TAX_DIR/taxonomy.qza \
 --p-exclude mitochondria,chloroplast,Feature \
 --o-filtered-table $TAX_DIR/chloro_mito_filtered_table.qza

echo "Filtering taxa contaminants from sequences, like mitochondria,chloroplast"
qiime taxa filter-seqs \
 --i-sequences $TMP_DIR/filtered_rarefied_rep_seq.qza \
 --i-taxonomy $TAX_DIR/taxonomy.qza \
 --p-exclude mitochondria,chloroplast,Feature \
 --o-filtered-sequences $TAX_DIR/chloro_mito_filtered_sequences.qza
  
# view the taxonomic composition of our samples with interactive bar plots
# correction for unequal library sizes is applied through table rarefy
  
echo "qiime taxa barplot"
qiime taxa barplot \
 --i-table $TAX_DIR/chloro_mito_filtered_table.qza \
 --i-taxonomy $TAX_DIR/taxonomy.qza \
 --m-metadata-file $TMP_DIR/metadata_file.txt \
 --o-visualization $TAX_DIR/taxa_bar_plots.qzv

# delete all uploaded data before copying to your local directory
/bin/rm $TMP_DIR/sample_filtered_rep_seq.qza
/bin/rm $TMP_DIR/representative_sequences.qza
/bin/rm $TMP_DIR/$TMP_DIR/metadata_file.txt
/bin/rm $TMP_DIR/db_otus.fasta
/bin/rm $TMP_DIR/db_otu_taxonomy.txt

# copy data
rsync -av $TMP_DIR/taxonomy/* $OUT_DIR

# Finally, delete tmp directory created in /scratch
/bin/rm -rf $TMP_DIR

# unload R, anaconda3 and qiime2
module unload R/3.5.2
module unload Anaconda3/5.2.0
source deactivate qiime2-2018.8

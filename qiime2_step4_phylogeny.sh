#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D                      #
# Emory University, Dept. of Human Genetics     #
# ashok.reddy.dinasarapu@emory.edu              #
# Date: 03/02/2019                              #
#################################################
 
#$ -N phylogeny
#$ -q b.q
#$ -pe smp 4
#$ -cwd
#$ -j y
#$ -m abe
#$ -M user_name@emory.edu
 
# activate qiime2 environment
module load R/3.5.2
module load Anaconda3/5.2.0
source activate qiime2-2019.1

# main project directory 
PROJ_DIR=$HOME/metagenomics
 
# create a directory and a sub-directory for output data
OUT_DIR=$PROJ_DIR/out/data_phylogeny
if [ ! -d ${OUT_DIR} ]; then
   mkdir -p ${OUT_DIR}
 fi
 
export TMPDIR=/scratch
 
if [ -e /bin/mktemp ]; then
  TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
  TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

chmod a+trwx $TMP_DIR
 
# Filtered data directory 
FILTER_DIR=$PROJ_DIR/out/data_filtered

# copy metadata, feature table and sequences
rsync -av $PROJ_DIR/data/ibs.microbiome/metadata_file.txt $TMP_DIR/metadata_file.txt
rsync -av $FILTER_DIR/sample_filtered_rep_seq.qza $TMP_DIR/sample_filtered_rep_seq.qza
rsync -av $FILTER_DIR/sample_filtered_table.qza $TMP_DIR/sample_filtered_table.qza

#########################################################
# 1. Tree for phylogenetic diversity analyses         	#
# Needed for 						#
# 	Alpha_rarefaction and Alpha_phylogenetic 	#
# 	Beta_rarefaction and Beta_phylogenetic		#
#########################################################

# create subdir in tmp
/bin/mkdir -p $TMP_DIR/phylogeny

# Perform de novo multiple sequence alignment using MAFFT
qiime alignment mafft \
 --i-sequences $TMP_DIR/sample_filtered_rep_seq.qza \
 --p-n-threads 4 \
 --o-alignment $TMP_DIR/phylogeny/aligned_rep_seqs.qza

# Mask (i.e., filter) unconserved and highly gapped columns from an alignment. 
# These positions are generally considered to add noise to a resulting phylogenetic tree.
qiime alignment mask \
 --i-alignment $TMP_DIR/phylogeny/aligned_rep_seqs.qza \
 --o-masked-alignment $TMP_DIR/phylogeny/masked_aligned_rep_seqs.qza

# Apply FastTree to generate a phylogenetic tree from the masked alignment.
qiime phylogeny fasttree \
 --i-alignment $TMP_DIR/phylogeny/masked_aligned_rep_seqs.qza \
 --p-n-threads 4 \
 --o-tree $TMP_DIR/phylogeny/unrooted_tree.qza

# final step in this section we apply midpoint rooting
qiime phylogeny midpoint-root \
 --i-tree $TMP_DIR/phylogeny/unrooted_tree.qza \
 --o-rooted-tree $TMP_DIR/phylogeny/rooted_tree.qza
  
# The diversity in a single sample (alpha diversity) is commonly measured using metrics such as 
# the Shannon index and the Chao1 estimator, while the variation between pairs of samples (beta diversity) 
# is measured using metrics such as the Jaccard distance or Bray-Curtis dissimilarity. 

# Typically you want to choose a value high enough that you capture the diversity present in samples with high counts, 
# but low enough that you don’t get rid of a ton of your samples. 
########################
# 2. Alpha Rarefaction #
########################

ALPHA_DIR=$TMP_DIR/alpha_rarefaction   
/bin/mkdir -p $ALPHA_DIR

# alpha rarefaction curves by computing rarefactions between `min_depth` and `max_depth`
  
qiime diversity alpha-rarefaction \
 --i-table $TMP_DIR/sample_filtered_table.qza \
 --i-phylogeny $TMP_DIR/phylogeny/rooted_tree.qza \
 --p-max-depth 20327 --p-min-depth 1 \
 --p-steps 20 --p-iterations 100 \
 --m-metadata-file $TMP_DIR/metadata_file.txt \
 --p-metrics shannon \
 --o-visualization $ALPHA_DIR/alpha_rarefaction.qzv

########################
# 3. Beta Rarefaction #
########################

BETA_DIR=$TMP_DIR/beta_rarefaction
/bin/mkdir -p $BETA_DIR/braycurtis
   
# Repeatedly rarefy a feature table to compare beta diversity results across and within rarefaction depths
# --p-iterations Number of times to rarefy the feature table at a given sampling depth

qiime diversity beta-rarefaction \
 --i-table $TMP_DIR/sample_filtered_table.qza \
 --i-phylogeny $TMP_DIR/phylogeny/rooted_tree.qza \
 --p-metric braycurtis \
 --p-sampling-depth 20327 --p-iterations 100 \
 --p-correlation-method spearman \
 --p-clustering-method nj \
 --m-metadata-file $TMP_DIR/metadata_file.txt \
 --o-visualization $BETA_DIR/braycurtis/beta_rarefaction.qzv

# delete uploaded data
/bin/rm $TMP_DIR/sample_filtered_table.qza
/bin/rm $TMP_DIR/sample_filtered_rep_seq.qza

# copy results
rsync -av $TMP_DIR/* $OUT_DIR

# delete tmp directory created in /scratch
/bin/rm -rf $TMP_DIR

# unload R, anaconda3 and qiime2
module unload R/3.5.2
module unload Anaconda3/5.2.0
source deactivate qiime2-2019.1

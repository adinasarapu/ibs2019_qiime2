#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D                      #
# Emory University, Dept. of Human Genetics     #
# ashok.reddy.dinasarapu@emory.edu              #
# Date: 03/02/2019                              #
#################################################

#$ -N filter
#$ -q b.q
#$ -pe smp 1
#$ -cwd
#$ -j y
#$ -m abe
#$ -M user_name@emory.edu

# activate qiime2 environment
module load R/3.5.2
module load Anaconda3/5.2.0
source activate qiime2-2019.1

# main project directory 
PROJ_DIR=$HOME/metagenomics

# create a directory and a sub-directory for output data
OUT_DIR=$PROJ_DIR/out/data_filtered
if [ ! -d ${OUT_DIR} ]; then
  mkdir -p ${OUT_DIR}
fi
 
export TMPDIR=/scratch

if [ -e /bin/mktemp ]; then
 TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

# Denoised data directory 
DENOISE_DIR=$PROJ_DIR/out/data_denoised

# copy
rsync -av $DENOISE_DIR/table.qza $TMP_DIR/table.qza
rsync -av $DENOISE_DIR/representative_sequences.qza $TMP_DIR/representative_sequences.qza 
rsync -av $PROJ_DIR/data/ibs.microbiome/metadata_file.txt $TMP_DIR/metadata_file.txt

#####################
# singletons filter #
#####################

qiime feature-table filter-features \
 --i-table $TMP_DIR/table.qza \
 --p-min-samples 2 \
 --o-filtered-table $TMP_DIR/singletons_filtered_table.qza

# visualize/summary singleton filtered table
qiime feature-table summarize \
 --i-table $TMP_DIR/singletons_filtered_table.qza \
 --o-visualization $TMP_DIR/singletons_filtered_table.qzv \
 --m-sample-metadata-file $TMP_DIR/metadata_file.txt

# filter seq
qiime feature-table filter-seqs \
 --i-table $TMP_DIR/singletons_filtered_table.qza \
 --i-data $TMP_DIR/representative_sequences.qza \
 --o-filtered-data $TMP_DIR/singletons_filtered_rep_seq.qza

#################
# Sample subset #
#################

qiime feature-table filter-samples \
 --i-table $TMP_DIR/singletons_filtered_table.qza \
 --m-metadata-file $TMP_DIR/metadata_file.txt \
 --p-where "SubjectGroup='A' OR SubjectGroup='B' OR SubjectGroup='Zymo'" \
 --o-filtered-table $TMP_DIR/sample_filtered_table.qza

#create filtered representative sequences based on new subset filtered table
qiime feature-table filter-seqs \
 --i-table $TMP_DIR/sample_filtered_table.qza \
 --i-data $TMP_DIR/representative_sequences.qza \
 --o-filtered-data $TMP_DIR/sample_filtered_rep_seq.qza

qiime feature-table summarize \
 --i-table $TMP_DIR/sample_filtered_table.qza \
 --o-visualization $TMP_DIR/sample_filtered_table.qzv \
 --m-sample-metadata-file $TMP_DIR/metadata_file.txt

/bin/rm $TMP_DIR/table.qza
/bin/rm $TMP_DIR/representative_sequences.qza
/bin/rm $TMP_DIR/metadata_file.txt 

rsync -av $TMP_DIR/* $OUT_DIR

/bin/rm -rf $TMP_DIR

# unload R, anaconda3 and qiime2
module unload R/3.5.2
module unload Anaconda3/5.2.0
source deactivate qiime2-2019.1
